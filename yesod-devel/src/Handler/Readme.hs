{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}

module Handler.Readme where
import Data.List as L
import Import
import Yesod.Form.Bootstrap3
import Yesod.Markdown
import Data.Text (Text)
import System.IO as IO
import qualified Data.Text as T
import Data.Either
import Text.Pandoc.Options
import Data.Tree
import System.Directory
import MdToTree
-- import Shelly
 --import System.File.Tree

data MdFile = MdFile
     { article :: Markdown}
-- Define our data that will be used for creating the form.

mdForm :: AForm Handler MdFile
mdForm = MdFile
      <$> areq markdownField (bfs ("Article" :: Text)) Nothing

getReadmeR :: Handler Html
getReadmeR = do
        let t = T.pack "static/readme.md"
        let picFolder = (T.pack $L.reverse $ T.unpack $ T.append ( (T.pack (L.reverse ("Pics")))) (T.tail $ T.dropWhile (/= '.') $ T.reverse t)):: Text
        let tList = (T.splitOn (T.pack "/" ) picFolder):: [Text]
        let baseFolderName = L.head $L.reverse $ tList
        fileContents <- (liftIO $ IO.readFile $T.unpack t)
        folder <-  (fileToAccordian fileContents)
        defaultLayout $ do
                $(widgetFile "md/new")

