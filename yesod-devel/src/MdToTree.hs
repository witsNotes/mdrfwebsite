{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TemplateHaskell #-}
module MdToTree(
        filepathToAccordian,
        fileToAccordian
) where

import Data.Either
import Prelude (fmap, show, sequence, Int(..), (+),(++),error, Ord, Eq, (==), (<=), String, Show, Bool, (.), ($), (<), (>), return)
import Data.List
import Data.Bool
import Yesod.Markdown
import System.IO
import Data.Tree
import qualified Import as I
import Import (widgetFile ,widgetToPageContent, Widget, Html, Handler, hamlet, lucius, withUrlRenderer, pageHead, toWidget, pageTitle, pageBody, newIdent)
import Text.Pandoc.Options
import qualified Data.Text as T
import Merges
import MarkupForTrees

filepathToAccordian :: String -> IO (Handler Html)
filepathToAccordian filepath = do
        file <- readFile filepath
        sequence $ map print (lines $ drawForest $ ( (map (fmap show) $ foldMdForest file)::Forest String))
        return (fileToAccordian file)

fileToAccordian :: String -> Handler Html
fileToAccordian s = handlerListConcat $ map (makeTreeFolder 0) $ htmlHandlerForest where
        htmlHandlerForest = (map twoDmdTreeToHtmlTree $ foldMdForest s):: Forest (HtmlHandlerForTree)
        

twoDmdTreeToHtmlTree :: Tree MdForTree -> Tree (HtmlHandlerForTree)
twoDmdTreeToHtmlTree (Node x xs) = Node (mdToHtmlHandlerForTree x) $ map twoDmdTreeToHtmlTree xs where
        mdToHtmlHandlerForTree :: MdForTree -> HtmlHandlerForTree
        mdToHtmlHandlerForTree (Heading x) = HtmlHeading $ genHtmlHandler $ mdStringtoHtml x
        mdToHtmlHandlerForTree (Line x ) = HtmlLine $ genHtmlHandler $ mdStringtoHtml x
        mdToHtmlHandlerForTree (Indent x) = HtmlIndent (map twoDmdTreeToHtmlTree x) where
                
genHtmlHandler :: Html -> Handler Html
genHtmlHandler text = do
        myLayout $ do
            toWidget [hamlet| #{text}|]

makeTreeFolder :: Int -> Tree (HtmlHandlerForTree) -> Handler Html 
makeTreeFolder n (Node x []) = forTreeToHandler n x
makeTreeFolder n (Node x xs) = makeFolder (n) (forTreeToHandler n x) $ handlerListConcat $ map (makeTreeFolder (n)) xs

forTreeToHandler :: Int -> HtmlHandlerForTree -> Handler Html
forTreeToHandler n (HtmlHeading x) = x
forTreeToHandler n (HtmlLine x) = x
forTreeToHandler n (HtmlIndent x) = handlerListConcat $ map (makeTreeBox (n)) x

makeTreeBox :: Int -> Tree (HtmlHandlerForTree) -> Handler Html
--makeTreeBox n (Node x []) = forTreeToHandler n x
makeTreeBox n (Node x xs) = makeBox (n) (forTreeToHandler n x) $ handlerListConcat $ map (makeTreeFolder (n+1)) xs

colors :: [String]
colors = cycle ["color1", "color2", "color3"]

makeBox :: Int -> Handler Html -> Handler Html -> Handler Html
makeBox n title body = do
        newId <- newIdent
        text <- body
        title' <- title
        let bColor = colors!!n 
        myLayout $ do
                $(widgetFile "md/box")

makeFolder :: Int -> Handler Html -> Handler Html -> Handler Html
makeFolder n title body = do
        newId <- newIdent
        text <- body
        title' <- title
        myLayout $ do
                $(widgetFile "md/folder")

handlerConcat :: Handler Html -> Handler Html -> Handler Html
handlerConcat a b = do
        a' <- a
        b' <- b
        withUrlRenderer ([hamlet| #{a'}|] I.++ [hamlet|
        
        |] I.++ [hamlet| #{b'}|])
myLayout :: Widget -> Handler Html
myLayout widget = do
        pc <- widgetToPageContent $ do
                widget
                toWidget [lucius| body { font-family: verdana } |]
        withUrlRenderer
                [hamlet|
                        $doctype 5
                        <html>
                          <head>
                           <title>#{pageTitle pc}
                           <meta charset=utf-8>
                              ^{pageHead pc}
                           <body>
                              <article>
                                ^{pageBody pc}
                |]

handlerListConcat :: [Handler Html] -> Handler Html
handlerListConcat xs = foldl handlerConcat (do withUrlRenderer ([hamlet||])) xs

