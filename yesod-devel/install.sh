#!/bin/bash
if [ ! -d ~/Desktop/fdmdApp ]; then
mkdir ~/Desktop/fdmdApp;
fi

cp -r static ~/Desktop/fdmdApp
cp -r config ~/Desktop/fdmdApp
cp dist/bin/fdmd ~/Desktop/fdmdApp
