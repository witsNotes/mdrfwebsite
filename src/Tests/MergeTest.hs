module Tests.MergeTest(
main
) where

import Merges
import System.IO
import Prelude

--main :: [IO()]
main = do
    f <- readFile "merges.md"
    let mdLines = mergeIndent . mergeSource $ lines f
    sequence $ map print mdLines 
