{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
module Handler.GitPulling where


import Import
import Yesod.GitRepo
import System.Directory
import System.File.Tree (getDirectory, copyTo_)

copyDirectory :: FilePath -> FilePath -> IO ()
copyDirectory source target = getDirectory source >>= copyTo_ target


cloneRepo :: Text -> Text -> Text -> IO (GitRepo ())
cloneRepo url branch folder = do
    repo <- gitRepo 
        url
        branch
        $ (\fp ->  copyDirectory fp $ "upload/" ++ (unpack folder))
    return repo


getGitPullingR :: Handler Html
getGitPullingR = do
       url <- runInputGet $ ireq textField "url"
       directory <- runInputGet $ ireq textField "dName"
       branch <- runInputGet $ ireq textField "branch"
       r <- liftIO ( cloneRepo  url branch directory)
       defaultLayout $ do
                toWidget[hamlet|The repository has been added|]
