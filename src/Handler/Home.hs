{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE QuasiQuotes #-}

module Handler.Home where

import System.Directory
import Yesod.Markdown
import Data.List as L
import System.IO as IO
import qualified Data.Text as T
import Import
import Yesod.Form.Bootstrap3
import Text.Julius (RawJS (..))
--import Yesod.GitRepo
import MdToTree
import System.File.Tree (getDirectory, copyTo_)
import FileLister


-- Define our data that will be used for creating the form.
data FileForm = FileForm
    { fileInfo :: FileInfo
    , fileDescription :: Text
    }

-- This is a handler function for the GET request method on the HomeR
-- resource pattern. All of your resource patterns are defined in
-- config/routes
--
-- The majority of the code you will write in Yesod lives in these handler
-- functions. You can spread them across multiple files if you are so
-- inclined, or create a single monolithic file.  

-- Define our data that will be used for creating the form.  



getHomeR :: Handler Html 
getHomeR = do
        master <- getYesod
--        fromRepo <- liftIO $ fmap grContent $ cloneRepo
        let files = fileLister "upload"
        let t = T.pack "static/readme.md"
        let picFolder = (T.pack $L.reverse $ T.unpack $ T.append ( (T.pack (L.reverse ("Pics")))) (T.tail $ T.dropWhile (/= '.') $ T.reverse t)):: Text
        let tList = (T.splitOn (T.pack "/" ) picFolder):: [Text]
        let baseFolderName = L.head $L.reverse $ tList
        fileContents <- (liftIO $ IO.readFile $T.unpack t)
        let pdfPath = genPdfPath (T.unpack t)
        folder <-  (fileToAccordian "files" fileContents)
        defaultLayout $ do
                $(widgetFile "md/new")

genPdfPath:: String -> String
genPdfPath s = "files" L.++  ( L.drop 6 $ (L.reverse $ L.drop 2 $ L.reverse s) L.++ "pdf")

postHomeR :: Handler Html
postHomeR = do
        let files = fileLister "upload"
	let submission = Nothing :: Maybe FileForm
        let t = T.pack "static/readme.md"
        let picFolder = (T.pack $L.reverse $ T.unpack $ T.append ( (T.pack (L.reverse ("Pics")))) (T.tail $ T.dropWhile (/= '.') $ T.reverse t)):: Text
        let tList = (T.splitOn (T.pack "/" ) picFolder):: [Text]
        let pdfPath = genPdfPath (T.unpack t)
        let baseFolderName = L.head $L.reverse $ tList
        fileContents <- (liftIO $ IO.readFile $T.unpack t)
        folder <-  (fileToAccordian "files" fileContents)
        defaultLayout $ do
                $(widgetFile "md/new")

