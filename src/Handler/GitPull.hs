{-# LANGUAGE TemplateHaskell #-}
module Handler.GitPull where

import Import
getGitPullR :: Handler Html
getGitPullR = do
  defaultLayout $ do
    $(widgetFile "gitPull")

