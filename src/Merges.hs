module Merges(
        mergeSource,
        mergeIndent,
        mergeLines,
        isHeading,
        indent
        )where
import Data.List

mergeSource :: [String] -> [String]
mergeSource [] = []
mergeSource strings = (takeWhileNotCode strings) ++ (takeFirstCode strings) :(mergeSource (dropFirstCode strings)) where

        takeWhileNotCode :: [String]-> [String]
        takeWhileNotCode xs = takeWhile  (not . codeDelim) xs

        takeFirstCode:: [String] -> String
        takeFirstCode xs
                |codeAndTail == [] = []
                |otherwise = unlines $ head codeAndTail : ((takeWhile (not. codeDelim) codeAndTail ) ++ ["```"])  where
                        codeAndTail = dropWhile  (not . codeDelim) xs

        dropFirstCode::[String] -> [String]
        dropFirstCode xs
                |(afterCode tailAndCode) == [] = []
                |otherwise = tail $ afterCode tailAndCode where
                        afterCode ys
                                | ys == [] =[]
                                | otherwise = dropWhile (not . codeDelim) $ tail ys 

                        tailAndCode = dropWhile (not .codeDelim) xs 

codeDelim :: String -> Bool
codeDelim s
        | length s < 3 = False
        | take 3 s == "```" = True
        | otherwise = False

mergeIndent :: [String] -> [String]
mergeIndent [] = []
mergeIndent strings = (takeWhileNotIndent strings) ++ (takeFirstIndentBlock strings) :(mergeIndent (dropFirstIndentBlock strings)) where

        takeWhileNotIndent :: [String]-> [String]
        takeWhileNotIndent xs = takeWhile  (not . indent) xs

        takeFirstIndentBlock :: [String] -> String
        takeFirstIndentBlock xs = unlines $ takeWhile (indent) $ dropWhile (not. indent) xs 

        dropFirstIndentBlock ::[String] -> [String]
        dropFirstIndentBlock xs = dropWhile indent $ dropWhile  (not .indent) xs

indent :: String -> Bool
indent s
        | length s == 0 = False
        | head s == '>' = True 
	| head s == ' '= indent $ tail s
        | otherwise = False

isHeading :: String -> Bool
isHeading "" = False 
isHeading s = s!!0 == '#' --still numbered bullet problim 

mergeLines:: [String] -> [String]
mergeLines xs = map unlines $  groupedLines where

        groupedLines :: [[String]]
        groupedLines = groupBy areBothLines xs

        areBothLines :: String -> String -> Bool
        areBothLines s s' = isLine s && isLine s' where

        isLine :: String -> Bool
        isLine x = ((not $ indent x) && (not $ isHeading x))
