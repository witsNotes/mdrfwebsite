{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE QuasiQuotes #-}

module FileLister(
  fileLister
  ) where

import System.Directory
import Yesod.Markdown
import Data.List as L
import System.IO as IO
import qualified Data.Text as T
import Yesod.Form.Bootstrap3
import Text.Julius (RawJS (..))
import Import
import MdToTree

fileLister :: String ->  Widget
fileLister dir = do
  files <- (liftIO $ fileListerS dir)::WidgetFor App [String]
  (concatFolderList dir files)::Widget 
  (concatFileList dir files)::Widget

concatFolderList ::String -> [String] -> Widget
concatFolderList dir xs = do
  list <- liftIO $ sequence $ L.map (doesDirectoryExist . ((dir  L.++ "/") L.++ )) xs
  let zlist = L.zip list xs 
  let flist = L.filter (fst) zlist
  let dlist = L.filter (\x -> (L.head x) /= '.') $ L.map snd flist
  let wlist = L.map (makeDirectoryWidget dir) dlist
  w <- (mconcat wlist)::Widget
  return $ w

concatFileList ::String -> [String] -> Widget
concatFileList dir xs = do
  list <- liftIO $ sequence $ L.map (doesFileExist . ((dir L.++ "/") L.++)) xs
  let zlist = L.zip list xs 
  let flist = L.filter (fst) zlist
  let dlist = L.map snd flist
  let wlist = L.map (makeFileWidget dir) dlist
  w <- (mconcat wlist)::Widget
  return $ w

-- makeFolderList :: [String] -> [Widget]
-- makeFolderList xs = do
--   w <-  (liftIO $ sequence $ listFolders xs)::[[String]]
--   return $  wlist
  -- widgets <- L.map (makeFileWidget "test") folderList
  -- return widgets 

makeList :: [String] -> [Widget] 
makeList xs = L.map (makeFileWidget "upload") xs

makeFileWidget:: String->String -> Widget
makeFileWidget dir file = do 
  toWidget [hamlet|<form method=get action=@{MdViewerR}> 
                       <input type=text name=directory value="#{dir}" hidden> 
                       <input type=text name=file value="#{file}" hidden >
                       <button .myButton type="submit">#{file}
             |]

makeDirectoryWidget:: String -> String -> Widget
makeDirectoryWidget dir dirHead = do 
  let files =  fileLister (dir L.++ "/" L.++ dirHead)
  newId <- newIdent
  let newId' = T.concat [newId , (T.pack dirHead) ]
  toWidget [whamlet|
                    <div .myButton type="button" onclick="myFunction('#{newId}')">#{dirHead}

                    <div class="container" id="#{newId}" .noDisplay x.style.display="none">
                        ^{files}
             |]
  
fileListerS :: String -> IO [String]
fileListerS dir = listDirectory dir

