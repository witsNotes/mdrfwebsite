# Installing

Use git to clone the source code. Then copy the config and static folder alon side the executable under dist (fdmd) to a folder called fdmdApp.

# Running

Go to the folder called fdmdApp and run the executable

```{bash}
./fdmd
```

Then open a webbrowser and go to 

localhost:3000/md/readme

Then type in the file you want to open in the box.

the file must have an accompioning Pics folder.

ie a file named foo.md must have a Pics folder name fooPics in the same directory.


